% Filename: transfer_beacon_packet.m
% Mission: IDEASSat
% Description: 
% Converts raw data fields in IDEASSat beacon packets to final human readable 
% form ready for display.
%
% Original Developer: Owen Kai-Lun Cheng, owen851201@gmail.com
% Project Manager: Loren Chang, loren@g.ncu.edu.tw
%
% Department of Space Science and Engineering
% National Central University, Taiwan
%
% 2021-01-28: Public release (Approved: L. Chang).
% 2021-02-04: Corrected conversion factor for 3 ADCS_VELOCITY_WRT_ECEF2 components (Approved: L. Chang).
%

function beacon_packet=transfer_beacon_packet(beacon_packet,short)

%---------------------------------------------------------------%
% main
beacon_packet.main.packet_time=ID_time_st(beacon_packet.main.TAI_SECONDS);
beacon_packet.main.EPS_Battery_SOC=transfer_SOC(beacon_packet.main.EPS_Battery_VOC);
switch beacon_packet.main.Current_spacecraft_mode
    case 0
        beacon_packet.main.Current_spacecraft_mode='Boot Mode';
    case 1
        beacon_packet.main.Current_spacecraft_mode='Phoenix Mode';
    case 2
        beacon_packet.main.Current_spacecraft_mode='Safe Mode';
    case 3
        beacon_packet.main.Current_spacecraft_mode='Charging Mode';
    case 4
        beacon_packet.main.Current_spacecraft_mode='Science Mode';
    case 5
        beacon_packet.main.Current_spacecraft_mode='Transition Mode';
    case 6
        beacon_packet.main.Current_spacecraft_mode='Test Mode';
end

if beacon_packet.main.ECLIPSE == 1
    beacon_packet.main.ECLIPSE='Yes';
elseif beacon_packet.main.ECLIPSE == 0
    beacon_packet.main.ECLIPSE='No';
else
    beacon_packet.main.ECLIPSE='Error';
end

if beacon_packet.main.Antenna_n == 1
    beacon_packet.main.Antenna_n='Burn';
elseif beacon_packet.main.Antenna_n == 0
    beacon_packet.main.Antenna_n='No';
else
    beacon_packet.main.Antenna_n='Error';
end

if beacon_packet.main.Solar_n == 1
    beacon_packet.main.Solar_n='Burn';
elseif beacon_packet.main.Solar_n == 0
    beacon_packet.main.Solar_n='No';
else
    beacon_packet.main.Solar_n='Error';
end

if beacon_packet.main.Nominal_mode == 1
    beacon_packet.main.Nominal_mode='Yes';
elseif beacon_packet.main.Nominal_mode == 0
    beacon_packet.main.Nominal_mode='No';
else
    beacon_packet.main.Nominal_mode='Error';
end

if beacon_packet.main.ADCS_Time_Valid == 1
    beacon_packet.main.ADCS_Time_Valid='Yes';
elseif beacon_packet.main.ADCS_Time_Valid == 0
    beacon_packet.main.ADCS_Time_Valid='No';
else
    beacon_packet.main.ADCS_Time_Valid='Error';    
end

if beacon_packet.main.ADCS_Refs_Valid == 1
    beacon_packet.main.ADCS_Refs_Valid='Yes';
elseif beacon_packet.main.ADCS_Refs_Valid == 0
    beacon_packet.main.ADCS_Refs_Valid='No';
else
    beacon_packet.main.ADCS_Refs_Valid='Error';
end

if beacon_packet.main.ADCS_Attitude_Valid == 1 
    beacon_packet.main.ADCS_Attitude_Valid='Yes';
elseif beacon_packet.main.ADCS_Attitude_Valid == 0 
    beacon_packet.main.ADCS_Attitude_Valid='No';
else
    beacon_packet.main.ADCS_Attitude_Valid='Error';
end

if beacon_packet.main.ADCS_MODE == 1
    beacon_packet.main.ADCS_MODE='FINE REF POINT';
elseif beacon_packet.main.ADCS_MODE == 0
    beacon_packet.main.ADCS_MODE='SUN POINT';
else
    beacon_packet.main.ADCS_MODE='Error';
end

switch beacon_packet.main.SAFE_MODE_REASON
    case 1 
        beacon_packet.main.SAFE_MODE_REASON='Boot';
    case 2 
        beacon_packet.main.SAFE_MODE_REASON='Command';
    case 3
        beacon_packet.main.SAFE_MODE_REASON='Attitude Invalid';
    case 8
        beacon_packet.main.SAFE_MODE_REASON='Time Invalid';
    case 12 
        beacon_packet.main.SAFE_MODE_REASON='Attitude Time Invalid';
    case 16
        beacon_packet.main.SAFE_MODE_REASON='Refs Invalid';
end

if beacon_packet.main.RECOMMEND_SUN_POINT == 1
    beacon_packet.main.RECOMMEND_SUN_POINT='Yes';
elseif beacon_packet.main.RECOMMEND_SUN_POINT == 0
    beacon_packet.main.RECOMMEND_SUN_POINT='No';
else
    beacon_packet.main.RECOMMEND_SUN_POINT='Error';
end

switch beacon_packet.main.ATT_STATUS
    case 0
        beacon_packet.main.ATT_STATUS='OK';
    case 1
        beacon_packet.main.ATT_STATUS='Pending';
    case 2
        beacon_packet.main.ATT_STATUS='Bad';
    case 3
        beacon_packet.main.ATT_STATUS='Too Few Stars';
    case 4
        beacon_packet.main.ATT_STATUS='Quest Failed';
    case 5
        beacon_packet.main.ATT_STATUS='Residuals Too High';
    case 25
        beacon_packet.main.ATT_STATUS='Rate Too High';
end

switch beacon_packet.main.SUN_POINT_STATE
    case 2
        beacon_packet.main.SUN_POINT_STATE='Search Init';
    case 3 
        beacon_packet.main.SUN_POINT_STATE='Searching';
    case 4
        beacon_packet.main.SUN_POINT_STATE='Waiting';
    case 5 
        beacon_packet.main.SUN_POINT_STATE='Converging';
    case 6 
        beacon_packet.main.SUN_POINT_STATE='On Sun';
    case 7 
        beacon_packet.main.SUN_POINT_STATE='Not Active';
end

if beacon_packet.main.GPS_VALID == 1
    beacon_packet.main.GPS_VALID='Yes';
elseif beacon_packet.main.GPS_VALID == 0
    beacon_packet.main.GPS_VALID='No';
else
    beacon_packet.main.GPS_VALID='Error';
end

if beacon_packet.cdh.CDH_Battery_Heater_ON_Status == 1
    beacon_packet.cdh.CDH_Battery_Heater_ON_Status='ON';
elseif beacon_packet.cdh.CDH_Battery_Heater_ON_Status == 0
    beacon_packet.cdh.CDH_Battery_Heater_ON_Status='OFF';
else
    beacon_packet.cdh.CDH_Battery_Heater_ON_Status='Error';
end

if beacon_packet.cdh.SD_CARD0_WORKING == 1
    beacon_packet.cdh.SD_CARD0_WORKING='ON';
elseif beacon_packet.cdh.SD_CARD0_WORKING == 0
    beacon_packet.cdh.SD_CARD0_WORKING='OFF';
else
    beacon_packet.cdh.SD_CARD0_WORKING='Error';
end

if beacon_packet.cdh.SD_CARD1_WORKING == 1
    beacon_packet.cdh.SD_CARD1_WORKING='ON';
elseif beacon_packet.cdh.SD_CARD1_WORKING == 1
    beacon_packet.cdh.SD_CARD1_WORKING='OFF';
else
    beacon_packet.cdh.SD_CARD1_WORKING='Error';
end

if beacon_packet.cdh.CDH_CIP_ON_Status == 1
    beacon_packet.cdh.CDH_CIP_ON_Status='ON';
elseif beacon_packet.cdh.CDH_CIP_ON_Status == 0
    beacon_packet.cdh.CDH_CIP_ON_Status='OFF';
else
    beacon_packet.cdh.CDH_CIP_ON_Status='Error';
end

if beacon_packet.cdh.CDH_SBAND_ON_Status == 1
    beacon_packet.cdh.CDH_SBAND_ON_Status='ON';
elseif beacon_packet.cdh.CDH_SBAND_ON_Status == 0
    beacon_packet.cdh.CDH_SBAND_ON_Status='OFF';
else
    beacon_packet.cdh.CDH_SBAND_ON_Status='Error';
end

if beacon_packet.cdh.CDH_GPS_ON_Status == 1
    beacon_packet.cdh.CDH_GPS_ON_Status='ON';
elseif beacon_packet.cdh.CDH_GPS_ON_Status == 0
    beacon_packet.cdh.CDH_GPS_ON_Status='OFF';
else
    beacon_packet.cdh.CDH_GPS_ON_Status='Error';
end

if beacon_packet.cdh.CDH_ADCS_ON_Status == 1
    beacon_packet.cdh.CDH_ADCS_ON_Status='ON';
elseif beacon_packet.cdh.CDH_ADCS_ON_Status == 0
    beacon_packet.cdh.CDH_ADCS_ON_Status='OFF';
else
    beacon_packet.cdh.CDH_ADCS_ON_Status='Error';
end

if beacon_packet.cdh.CDH_UHF_ON_Status == 1
    beacon_packet.cdh.CDH_UHF_ON_Status='ON';
elseif beacon_packet.cdh.CDH_UHF_ON_Status == 0
    beacon_packet.cdh.CDH_UHF_ON_Status='OFF';
else
    beacon_packet.cdh.CDH_UHF_ON_Status='Error';
end

beacon_packet.eps.LAST_EPS_UHF_Tx_Current=double(beacon_packet.eps.LAST_EPS_UHF_Tx_Current)/2;

if(~short)
    beacon_packet.main.PAST_RECOMMEND_SUN_POINT=dec2bin(beacon_packet.main.PAST_RECOMMEND_SUN_POINT,8);
    beacon_packet.main.PAST_GPS_VALID=dec2bin(beacon_packet.main.PAST_GPS_VALID,8);

    beacon_packet.main.PAST_EPS_PV2_Voltage1=double(beacon_packet.main.PAST_EPS_PV2_Voltage1)/1000;
    beacon_packet.main.PAST_EPS_PV0_Voltage1=double(beacon_packet.main.PAST_EPS_PV0_Voltage1)/1000;
    beacon_packet.main.PAST_EPS_PV1_Voltage1=double(beacon_packet.main.PAST_EPS_PV1_Voltage1)/1000;
    beacon_packet.main.PAST_EPS_PV2_Voltage2=double(beacon_packet.main.PAST_EPS_PV2_Voltage2)/1000;
    beacon_packet.main.PAST_EPS_PV0_Voltage2=double(beacon_packet.main.PAST_EPS_PV0_Voltage2)/1000;
    beacon_packet.main.PAST_EPS_PV1_Voltage2=double(beacon_packet.main.PAST_EPS_PV1_Voltage2)/1000;

%---------------------------------------------------------------%
% CDH
    beacon_packet.cdh.CDH_Last_Command_APID=char(beacon_packet.cdh.CDH_Last_Command_APID);

    switch(beacon_packet.cdh.SD_CARD0_REASON)
        case 0
          beacon_packet.cdh.SD_CARD0_REASON='IDLE';
        case 11
            beacon_packet.cdh.SD_CARD0_REASON='SUCCESS';
        case -10
            beacon_packet.cdh.SD_CARD0_REASON='INIT ERROR';
        case -11
            beacon_packet.cdh.SD_CARD0_REASON='WRITE ERROR';
        case -12
            beacon_packet.cdh.SD_CARD0_REASON='READ ERROR';
        case -13
            beacon_packet.cdh.SD_CARD0_REASON='ADDR OUT OF RANGE';
        case -14
            beacon_packet.cdh.SD_CARD0_REASON='USE PROTECT ADDR';
        case -15
            beacon_packet.cdh.SD_CARD0_REASON='CLOSE ERROR';
        case -16
            beacon_packet.cdh.SD_CARD0_REASON='GetSat ERROR';
    end
    
    switch(beacon_packet.cdh.SD_CARD1_REASON)
        case 0
          beacon_packet.cdh.SD_CARD1_REASON='IDLE';
        case 11
            beacon_packet.cdh.SD_CARD1_REASON='SUCCESS';
        case -10
            beacon_packet.cdh.SD_CARD1_REASON='INIT ERROR';
        case -11
            beacon_packet.cdh.SD_CARD1_REASON='WRITE ERROR';
        case -12
            beacon_packet.cdh.SD_CARD1_REASON='READ ERROR';
        case -13
            beacon_packet.cdh.SD_CARD1_REASON='ADDR OUT OF RANGE';
        case -14
            beacon_packet.cdh.SD_CARD1_REASON='USE PROTECT ADDR';
        case -15
            beacon_packet.cdh.SD_CARD1_REASON='CLOSE ERROR';
        case -16
            beacon_packet.cdh.SD_CARD1_REASON='GetSat ERROR';
    end


    switch(beacon_packet.cdh.UHF_REASON)
        case 0
            beacon_packet.cdh.UHF_REASON='Normal';
        case 1
            beacon_packet.cdh.UHF_REASON='Voltage Error';
        case 2
            beacon_packet.cdh.UHF_REASON='UHF Error';
        case 3
            beacon_packet.cdh.UHF_REASON='AFC Close';
        case 4
            beacon_packet.cdh.UHF_REASON='Frequency Error';
        case 5
            beacon_packet.cdh.UHF_REASON='SWD Close';
    end
    
    switch(beacon_packet.cdh.ADCS_GPS_REASON)
        case 0
            beacon_packet.cdh.ADCS_GPS_REASON='Normal';
        case 1
            beacon_packet.cdh.ADCS_GPS_REASON='Voltage Error';
    end
    
    switch(beacon_packet.cdh.CIP_REASON)
        case 0
            beacon_packet.cdh.CIP_REASON='Normal';
        case 1
            beacon_packet.cdh.CIP_REASON='Voltage / Current Error';
        case 2
            beacon_packet.cdh.CIP_REASON='Temperature Error';
    end
%---------------------------------------------------------------%
% EPS
    beacon_packet.eps.EPS_Solar_Panel0_Temperature=convert_temp(beacon_packet.eps.EPS_Solar_Panel0_Temperature, ...
        beacon_packet.eps.EPS_CDH_Voltage,0);
    beacon_packet.eps.EPS_Battery_Temperature1=convert_temp(beacon_packet.eps.EPS_Battery_Temperature1, ...
        beacon_packet.eps.EPS_CDH_Voltage,1);
    beacon_packet.eps.EPS_Solar_Panel1_Temperature=convert_temp(beacon_packet.eps.EPS_Solar_Panel1_Temperature, ...
        beacon_packet.eps.EPS_CDH_Voltage,0);
    beacon_packet.eps.EPS_Battery_Temperature2=convert_temp(beacon_packet.eps.EPS_Battery_Temperature2, ...
        beacon_packet.eps.EPS_CDH_Voltage,1);
    beacon_packet.eps.EPS_Solar_Panel2_Temperature=convert_temp(beacon_packet.eps.EPS_Solar_Panel2_Temperature, ...
        beacon_packet.eps.EPS_CDH_Voltage,0);
    beacon_packet.eps.EPS_CDH_Temperature=convert_temp(beacon_packet.eps.EPS_CDH_Temperature, ...
        beacon_packet.eps.EPS_CDH_Voltage,0);
    beacon_packet.eps.EPS_Temperature=convert_temp(beacon_packet.eps.EPS_Temperature, ...
        beacon_packet.eps.EPS_CDH_Voltage,0);

    beacon_packet.eps.EPS_CDH_Current=double(beacon_packet.eps.EPS_CDH_Current)/2;
    beacon_packet.eps.EPS_CDH_Voltage=double(beacon_packet.eps.EPS_CDH_Voltage)/1000;
    beacon_packet.eps.EPS_GNSS_Current=double(beacon_packet.eps.EPS_GNSS_Current)/2;
    beacon_packet.eps.EPS_GNSS_Voltage=double(beacon_packet.eps.EPS_GNSS_Voltage)/1000;
    beacon_packet.eps.EPS_UHF_Current=double(beacon_packet.eps.EPS_UHF_Current)/2;
    beacon_packet.eps.EPS_UHF_Voltage=double(beacon_packet.eps.EPS_UHF_Voltage)/1000;
    beacon_packet.eps.EPS_PV2_Current=double(beacon_packet.eps.EPS_PV2_Current)/2;
    beacon_packet.eps.EPS_PV2_Voltage=double(beacon_packet.eps.EPS_PV2_Voltage)/1000;
    beacon_packet.eps.EPS_PV0_Current=double(beacon_packet.eps.EPS_PV0_Current)/2;
    beacon_packet.eps.EPS_PV0_Voltage=double(beacon_packet.eps.EPS_PV0_Voltage)/1000;
    beacon_packet.eps.EPS_PV1_Current=double(beacon_packet.eps.EPS_PV1_Current)/2;
    beacon_packet.eps.EPS_PV1_Voltage=double(beacon_packet.eps.EPS_PV1_Voltage)/1000;
    beacon_packet.eps.EPS_Battery_CHG_Current=double(beacon_packet.eps.EPS_Battery_CHG_Current)/2;
    beacon_packet.eps.EPS_Battery_CHG_Voltage=double(beacon_packet.eps.EPS_Battery_CHG_Voltage)/1000;
    beacon_packet.eps.EPS_Heater_Current=double(beacon_packet.eps.EPS_Heater_Current)/2;
    beacon_packet.eps.EPS_Heater_Voltage=double(beacon_packet.eps.EPS_Heater_Voltage)/1000;
    beacon_packet.eps.EPS_ADCS_Current=double(beacon_packet.eps.EPS_ADCS_Current)/2;
    beacon_packet.eps.EPS_ADCS_Voltage=double(beacon_packet.eps.EPS_ADCS_Voltage)/1000;
    beacon_packet.eps.LAST_EPS_Sband_Current=double(beacon_packet.eps.LAST_EPS_Sband_Current)/2;
    beacon_packet.eps.LAST_EPS_Sband_Voltage=double(beacon_packet.eps.LAST_EPS_Sband_Voltage)/1000;
    beacon_packet.eps.LAST_EPS_CIP_Current=double(beacon_packet.eps.LAST_EPS_CIP_Current)/2;
    beacon_packet.eps.LAST_EPS_CIP_Voltage=double(beacon_packet.eps.LAST_EPS_CIP_Voltage)/1000;
    beacon_packet.eps.LAST_EPS_Sband_Tx_Current=double(beacon_packet.eps.LAST_EPS_Sband_Tx_Current)/2;

    %---------------------------------------------------------------%
    % UHF
    beacon_packet.uhf.UHF_frequency_Channel = char(beacon_packet.uhf.UHF_frequency_Channel);

    if (( beacon_packet.uhf.UHF_Temperature1 >= 48 && beacon_packet.uhf.UHF_Temperature1 <= 57 ) || ( beacon_packet.uhf.UHF_Temperature1 >= 65 && beacon_packet.uhf.UHF_Temperature1 <= 70 )) ...
            && (( beacon_packet.uhf.UHF_Temperature2 >= 48 && beacon_packet.uhf.UHF_Temperature2 <= 57 ) || ( beacon_packet.uhf.UHF_Temperature2 >= 65 && beacon_packet.uhf.UHF_Temperature2 <= 70 ))
        beacon_packet.uhf.UHF_Temperature=hex2dec([char(beacon_packet.uhf.UHF_Temperature1) char(beacon_packet.uhf.UHF_Temperature2)])*2;
    else
        beacon_packet.uhf.UHF_Temperature=-1;
    end
    if beacon_packet.uhf.UHF_Temperature>500
        beacon_packet.uhf.UHF_Temperature=-1;
    end
    beacon_packet.uhf.UHF_Echo_ON_status=char(beacon_packet.uhf.UHF_Echo_ON_status);
    beacon_packet.uhf.UHF_AFC_status=char(beacon_packet.uhf.UHF_AFC_status);
    beacon_packet.uhf.UHF_SWD_Announce=char(beacon_packet.uhf.UHF_SWD_Announce);
    beacon_packet.uhf.UHF_Readback_mode=char(beacon_packet.uhf.UHF_Readback_mode);

    %---------------------------------------------------------------%
    % SBAND
    if beacon_packet.sband.Sband_Last_Power_Good == 3
        beacon_packet.sband.Sband_Last_Power_Good_status='Normal';
    else
        beacon_packet.sband.Sband_Last_Power_Good_status='Abnormal';
    end
    beacon_packet.sband.Sband_Last_RF_Output_Power=double(beacon_packet.sband.Sband_Last_RF_Output_Power)*3/4096*28/18;
    beacon_packet.sband.Sband_Last_PA_Temperature=(double(beacon_packet.sband.Sband_Last_PA_Temperature)*3/4096-0.5)*100;
    beacon_packet.sband.Sband_Last_Top_Board_Temperature=double(bitshift(beacon_packet.sband.Sband_Last_Top_Board_Temperature,-4))*0.0625;
    beacon_packet.sband.Sband_Last_Bot_Board_Temperature=double(bitshift(beacon_packet.sband.Sband_Last_Bot_Board_Temperature,-4))*0.0625;

    %---------------------------------------------------------------%
    % CIP
    beacon_packet.cip.CIP_Command_Stat_from_GS=dec2hex(beacon_packet.cip.CIP_Command_Stat_from_GS);
    beacon_packet.cip.CIP_Temperature1=double(beacon_packet.cip.CIP_Temperature1)/128;
    beacon_packet.cip.CIP_Temperature2=double(beacon_packet.cip.CIP_Temperature2)/129;
    beacon_packet.cip.CIP_Temperature3=double(beacon_packet.cip.CIP_Temperature3)/130;

    %---------------------------------------------------------------%
    % ADCS
    switch beacon_packet.adcs.ADCS_Command_Status
        case 0
            beacon_packet.adcs.ADCS_Command_Status='OK';
        case 1
            beacon_packet.adcs.ADCS_Command_Status='Bad APID';
        case 2 
            beacon_packet.adcs.ADCS_Command_Status='Bad Opcode';
        case 3 
            beacon_packet.adcs.ADCS_Command_Status='Bad Data';
        case 7
            beacon_packet.adcs.ADCS_Command_Status='No CMD Data';
        case 8
            beacon_packet.adcs.ADCS_Command_Status='CMD SRVC Overrun';
        case 9
            beacon_packet.adcs.ADCS_Command_Status='CMD APID Overrun';
        case 12
            beacon_packet.adcs.ADCS_Command_Status='Tables Busy';
        case 13
            beacon_packet.adcs.ADCS_Command_Status='Flash Not Armed';
        case 14
            beacon_packet.adcs.ADCS_Command_Status='Thrusters Disabled';
        case 15
            beacon_packet.adcs.ADCS_Command_Status='ATT ERR Too High';
        case 16
            beacon_packet.adcs.ADCS_Command_Status='ASync Refused';
    end

    switch beacon_packet.adcs.ADCS_Command_Reject_status
        case 0
            beacon_packet.adcs.ADCS_Command_Reject_status='OK';
        case 1
            beacon_packet.adcs.ADCS_Command_Reject_status='Bad APID';
        case 2
            beacon_packet.adcs.ADCS_Command_Reject_status='Bad Opcode';
        case 3
            beacon_packet.adcs.ADCS_Command_Reject_status='Bad Data';
        case 7
            beacon_packet.adcs.ADCS_Command_Reject_status='No CMD Data';
        case 8
            beacon_packet.adcs.ADCS_Command_Reject_status='CMD SRVC Overrun';
        case 9
            beacon_packet.adcs.ADCS_Command_Reject_status='CMD APID Overrun';
        case 12
            beacon_packet.adcs.ADCS_Command_Reject_status='Tables Busy';
        case 13 
            beacon_packet.adcs.ADCS_Command_Reject_status='Flash Not Armed';
        case 14
            beacon_packet.adcs.ADCS_Command_Status='Thrusters Disabled';
        case 15
            beacon_packet.adcs.ADCS_Command_Status='ATT ERR Too High';
        case 16
            beacon_packet.adcs.ADCS_Command_Status='ASync Refused';
    end  


    beacon_packet.adcs.ADCS_POSITION_WRT_ECEF1=double(beacon_packet.adcs.ADCS_POSITION_WRT_ECEF1)*2E-5;
    beacon_packet.adcs.ADCS_POSITION_WRT_ECEF2=double(beacon_packet.adcs.ADCS_POSITION_WRT_ECEF2)*2E-5;
    beacon_packet.adcs.ADCS_POSITION_WRT_ECEF3=double(beacon_packet.adcs.ADCS_POSITION_WRT_ECEF3)*2E-5;
    beacon_packet.adcs.ADCS_VELOCITY_WRT_ECEF1=double(beacon_packet.adcs.ADCS_VELOCITY_WRT_ECEF1)*5E-9;
    beacon_packet.adcs.ADCS_VELOCITY_WRT_ECEF2=double(beacon_packet.adcs.ADCS_VELOCITY_WRT_ECEF2)*5E-9;
    beacon_packet.adcs.ADCS_VELOCITY_WRT_ECEF3=double(beacon_packet.adcs.ADCS_VELOCITY_WRT_ECEF3)*5E-9;
    beacon_packet.adcs.ADCS_SUN_VECTOR_BODY1=double(beacon_packet.adcs.ADCS_SUN_VECTOR_BODY1)*4E-5;
    beacon_packet.adcs.ADCS_SUN_VECTOR_BODY2=double(beacon_packet.adcs.ADCS_SUN_VECTOR_BODY2)*4E-5;
    beacon_packet.adcs.ADCS_SUN_VECTOR_BODY3=double(beacon_packet.adcs.ADCS_SUN_VECTOR_BODY3)*4E-5;

    beacon_packet.adcs.ADCS_Tracker_Detector_temperature=double(beacon_packet.adcs.ADCS_Tracker_Detector_temperature)*0.8;
    beacon_packet.adcs.ADCS_Ext_Temperature=double(beacon_packet.adcs.ADCS_Ext_Temperature)*0.005;
    beacon_packet.adcs.ADCS_Imu_Temperature=double(beacon_packet.adcs.ADCS_Imu_Temperature)*0.005;
end
end

function temperature=convert_temp(value,voltage,isbattery)
    value=double(value);
    voltage=double(voltage);
    Vsense=value*voltage/4096;
    Rth=23.2*Vsense/(voltage-Vsense);
    if isbattery
        temperature=1/((1/298)+((1/3518)*log(Rth/10)));
    else
        temperature=1/((1/298)+((1/3435)*log(Rth/10)));
    end
    temperature=temperature-273;
end

function SOC=transfer_SOC(VOC)
    VOC=double(VOC);
    SOC=VOC^5*1.227714187180328e-14-VOC^4*4.416081929830845e-10+VOC^3*6.321683675651610e-06 ...
        -VOC^2*0.045004731757696+VOC*1.593369976652199e+02-2.244805537889594e+05;
    SOC=round(SOC,2);
end
